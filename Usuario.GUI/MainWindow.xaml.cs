﻿using Base.COMMON.Entidades;
using Base.COMMON.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Usuario.BIZ;
using Usuario.DAL;

namespace Usuario.GUI
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IManejadorUsuario manejadorUsuario;
        public MainWindow()
        {
            InitializeComponent();
            manejadorUsuario = new ManejadorUsuarios(new RepositorioGenerico<Usuarios>());
            ActualizarTablaUsuarios();
        }

        private void ActualizarTablaUsuarios()
        {
            cmbUsuario.ItemsSource = null;
            cmbUsuario.ItemsSource = manejadorUsuario.Lista;
        }

        private void Ingresar_Click(object sender, RoutedEventArgs e)
        {
            if (cmbUsuario.Text == "")
            {
                MessageBox.Show("Aun no has ingresado el usuario", "Usuario", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(psbContrasena.Password))
            {
                MessageBox.Show("Aun no has ingresado una contrasena", "Usuario", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (cmbUsuario.SelectedItem != null)
            {
                Usuarios c = cmbUsuario.SelectedItem as Usuarios;
                if (psbContrasena.Password == c.contrasena)
                {
                    Principal x = new Principal();
                    x.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Contrasena incorrecta", "Usuario", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Aun no has seleccionado  usuario", "Usuario", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
