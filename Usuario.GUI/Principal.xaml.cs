﻿using Base.COMMON;
using Base.COMMON.Entidades;
using Base.COMMON.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Usuario.BIZ;
using Usuario.DAL;

namespace Usuario.GUI
{
    /// <summary>
    /// Lógica de interacción para Principal.xaml
    /// </summary>
    public partial class Principal : Window
    {

        enum Accion
        {
            Nuevo,
            Editar
        }

        Accion operacion;
        IManejadorDeporte manejadorDeportes;
        IManejadorEquipos manejadorEquipos;
        IManejadorUsuario manejadorUsuario;
        IManejadorTorneos manejadorTorneos;
        List<Aleatorios> lista = new List<Aleatorios>();
        List<Aleatorios> lista2 = new List<Aleatorios>();
        List<Torneos> torneos = new List<Torneos>();
        string equipoUno;
        string equipoDos;

        public Principal()
        {
            InitializeComponent();
            manejadorDeportes = new ManejadorDeportes(new RepositorioGenerico<Deportes>());
            manejadorEquipos = new ManejadorEquipos(new RepositorioGenerico<Equipos>());
            manejadorUsuario = new ManejadorUsuarios(new RepositorioGenerico<Usuarios>());
            manejadorTorneos = new ManejadorTorneos(new RepositorioGenerico<Torneos>());

            CargarTablas();
            ActualizarTablaDeportes();
            LimpiarCamposDeportes(false);
            ActualizarTablaEquipos();
            ActualizarTablaTorneos();
            LimpiarCamposEquipos(false);
            LimpiarTablasTorneo(false);
            ActualizarTablaUsuarios();
            HabilitarBotonesUsuario(false);
        }
        private void LimpiarTablasTorneo(bool a)
        {
            cmbTipoDeporteTorneo.ItemsSource = "";
            clFechaTorneo.Text = "";
            cmbTipoDeporteTorneo.IsEnabled = a;
            clFechaTorneo.IsEnabled = a;
            CargarTablas();
            btnTorneoOrdenar.IsEnabled = a;
            btnTorneoCancelar.IsEnabled = a;
            btnTorneoEliminar.IsEnabled = !a;
            btnTorneoNuevo.IsEnabled = !a;
        }

        private void CargarTablas()
        {
            cmbDeportePuntoEquipos.ItemsSource = null;
            cmbDeportePuntoEquipos.ItemsSource = manejadorDeportes.Lista;
            cmbTipoDeporteTorneo.ItemsSource = null;
            cmbTipoDeporteTorneo.ItemsSource = manejadorDeportes.Lista;
            dtgTab.ItemsSource = null;
            dtgTab.ItemsSource = manejadorTorneos.Lista;
            cmbDeportes.ItemsSource = null;
            cmbDeportes.ItemsSource = manejadorDeportes.Lista;
           
            
        }
        private void LimpiarCamposEquipos(bool v)
        {
            txbNombreEquipo.Clear();
            txbNombreEquipo.IsEnabled = v;
            txbNombreCapitan.Clear();
            txbNombreCapitan.IsEnabled = v;
            cmbDeportes.IsEnabled = v;
            btnDeporteCancelar.IsEnabled = v;
            btnDeporteGuardar.IsEnabled = v;
            btnDeporteNuevo.IsEnabled = !v;
            btnDeporteEditar.IsEnabled = !v;
            btnDeporteEliminar.IsEnabled = !v;
        }
        
        private void LimpiarCamposDeportes(bool a)
        {
            txbDeporte.Clear();
            txbDeporte.IsEnabled = a;
            btnCancelar.IsEnabled = a;
            btnGuardar.IsEnabled = a;
            btnNuevo.IsEnabled = !a;
            btnEditar.IsEnabled = !a;
            btnEliminar.IsEnabled = !a;
        }
        private void LimpiarCamposUsuarios()
        {
            txbNombreUsuario.Clear();
            txbContrasena.Clear();
        }

        private void HabilitarBotonesUsuario(bool v)
        {
            txbNombreUsuario.IsEnabled = v;
            txbContrasena.IsEnabled = v;
            btnUsuarioNuevo.IsEnabled = !v;
            btnUsuarioCancelar.IsEnabled = v;
            btnUsuarioGuardar.IsEnabled = v;
            btnUsuarioEditar.IsEnabled = !v;
            btnUsuarioEliminar.IsEnabled = !v;
        }

        private void ActualizarTablaDeportes()
        {
            dtgDeportes.ItemsSource = null;
            dtgDeportes.ItemsSource = manejadorDeportes.Lista;
        }
        private void ActualizarTablaEquipos()
        {
            dtgEquipos.ItemsSource = null;
            dtgEquipos.ItemsSource = manejadorEquipos.Lista;
            cmbDeportes.ItemsSource = null;
            cmbDeportes.ItemsSource = manejadorDeportes.Lista;
        }
        private void ActualizarTablaTorneos()
        {
            dtgTab.ItemsSource = null;
            dtgTab.ItemsSource = manejadorTorneos.Lista;
            cmbTipoDeporteTorneo.ItemsSource = null;
            cmbTipoDeporteTorneo.ItemsSource = manejadorDeportes.Lista;
            
        }
        private void ActualizarTablaUsuarios()
        {
            dtgUsuarios.ItemsSource = null;
            dtgUsuarios.ItemsSource = manejadorUsuario.Lista;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeportes(true);
            operacion = Accion.Nuevo;
        }
        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgDeportes.SelectedItem != null)
            {
                operacion = Accion.Editar;
                LimpiarCamposDeportes(true);
                Deportes k = dtgDeportes.SelectedItem as Deportes;
                txbDeporte.Text = k.Deporte;
            }
            else
            {
                MessageBox.Show("No se a seleccionado ningun campo para Editar", "Deportes", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbDeporte.Text))
            {
                MessageBox.Show("¡Error, no has igresado un deporte!", "Deporte", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (operacion == Accion.Nuevo)
            {
                Deportes dep = new Deportes();
                dep.Deporte = txbDeporte.Text;
                if (manejadorDeportes.Agregar(dep))
                {
                    MessageBox.Show("Deporte guardado con éxito", "Deporte", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeportes(true);
                    ActualizarTablaDeportes();

                }
                else
                {
                    MessageBox.Show("Error al guardar el deporte", "Deporte", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Deportes depo = dtgDeportes.SelectedItem as Deportes;
                depo.Deporte = txbDeporte.Text;
                if (manejadorDeportes.Modificar(depo))
                {
                    MessageBox.Show("Deporte guardado con éxito", "Deporte", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeportes(false);
                    //dtgDeportes.IsEnabled = false;
                    ActualizarTablaDeportes();

                }
                else
                {
                    MessageBox.Show("Error al guardar el deporte", "Deporte", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeportes(false);
            //dtgDeportes.IsEnabled = false;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Deportes m = dtgDeportes.SelectedItem as Deportes;
            if (m != null)
            {
                MessageBox.Show("Quieres eliminar a" + m.Deporte, "Deporte", MessageBoxButton.YesNo, MessageBoxImage.Question);
                manejadorDeportes.Eliminar(m.Id);
                ActualizarTablaDeportes();
            }
            else
            {
                MessageBox.Show("No has seleccionado ningun campo", "Deportes", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //__________________________________________________________________________________________________________________________

        private void btnDeporteNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposEquipos(true);
            operacion = Accion.Nuevo;
        }

        private void btnDeporteEditar_Click(object sender, RoutedEventArgs e)
        {
            Equipos b = dtgEquipos.SelectedItem as Equipos;
            if (b != null)
            {
                LimpiarCamposEquipos(true);
                txbNombreEquipo.Text = b.NombreEquipo;
                txbNombreCapitan.Text = b.NombreCapitan;
                cmbDeportes.Text = b.Deporte;
                operacion = Accion.Editar;

            }
            else
            {
                MessageBox.Show("No has seleccionado la tabla del equipo, selecciona un elemento", "Equipos", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDeporteGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbDeportes.Text) || string.IsNullOrEmpty(txbNombreEquipo.Text) || string.IsNullOrEmpty(txbNombreCapitan.Text))
            {
                MessageBox.Show("¡Error, no has igresado un equipo!", "Equipo", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (operacion == Accion.Nuevo)
            {
                Equipos k = new Equipos();
                k.Deporte = cmbDeportes.Text;
                k.NombreEquipo = txbNombreEquipo.Text;
                k.NombreCapitan = txbNombreCapitan.Text;
                if (manejadorEquipos.Agregar(k))
                {
                    MessageBox.Show("Equipo guardado con éxito", "Equipo", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablaEquipos();
                    LimpiarCamposEquipos(false);
                }
                else
                {
                    MessageBox.Show("Error al guardar el equipo", "Equipo", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Equipos k = dtgEquipos.SelectedItem as Equipos;
                k.Deporte = cmbDeportes.Text;
                k.NombreEquipo = txbNombreEquipo.Text;
                k.NombreEquipo = txbNombreCapitan.Text;
                if (manejadorEquipos.Modificar(k))
                {
                    MessageBox.Show("Equipo guardado con éxito", "Equipo", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    LimpiarCamposEquipos(false);
                    ActualizarTablaEquipos();
                }
                else
                {
                    MessageBox.Show("Error al guardar el equipo", "Equipo", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnDeporteCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposEquipos(false);
        }

        private void btnDeporteEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgEquipos.SelectedItem != null)
            {
                Equipos m = dtgEquipos.SelectedItem as Equipos;
                if (MessageBox.Show("Quieres eliminar a" + m.NombreEquipo, "Equipo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEquipos.Eliminar(m.Id))
                    {
                        MessageBox.Show("El equipo ha sido eliminado", "Equipos", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        ActualizarTablaEquipos();
                    }
                }
                else
                {
                    MessageBox.Show("No has seleccionado ningun campo", "Equipos", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        //__________________________________________________________________________________________________________________________

        private void btnTorneoNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarTablasTorneo(true);
           torneos = new List<Torneos>();
        }
        public void PrimerEquipoTorneo()
        {
            int contador = 0;
            Aleatorios a = new Aleatorios();
            foreach (var item in lista)
            {
                contador++;
                if (contador == 1)
                {
                    equipoUno = item.dato;
                    a.dato = item.dato;
                    lista.Remove(item);
                    lista2.Add(a);
                    break;
                }
            }

        }

        private void SegundoEquipotorneoImpar()
        {
            if (lista.Count >= 1)
            {
                Random r = new Random();
                int val = r.Next(1, lista.Count);
                int contador = 0;

                foreach (var item2 in lista)
                {
                    contador++;

                    if (contador == val)
                    {
                        equipoDos = item2.dato;
                        lista.Remove(item2);
                        break;
                    }
                }
            }
        }
        private void AgregarListaNumerosImpares(string palabra)
        {
            PrimerEquipoTorneo();
            SegundoEquipotorneoImpar();
            Torneos a = new Torneos()
            {
                EquipoUno = equipoUno,
                EquipoDos = equipoDos,
                Marc1 = 0,
                Marc2 = 0,
                Dep = palabra,
                Programado = clFechaTorneo.Text,
            };
            torneos.Add(a);
            manejadorTorneos.Agregar(a);
            dtgTab.ItemsSource = null;
            dtgTab.ItemsSource = manejadorTorneos.Lista;
        }

        private void LimpiarTorneo(bool a)
        {
            cmbTipoDeporteTorneo.ItemsSource = "";
            clFechaTorneo.Text = "";
            cmbTipoDeporteTorneo.IsEnabled = a;
            clFechaTorneo.IsEnabled = a;
            btnTorneoCancelar.IsEnabled = a;
            btnTorneoEliminar.IsEnabled = !a;
            btnTorneoNuevo.IsEnabled = !a;
            btnTorneoOrdenar.IsEnabled = a;
        }
        private void UltimoElemntoRestante(string palabra)
        {
            PrimerEquipoTorneo();
            if (lista.Count >= 1)
            {
                Random r = new Random();
                int val = r.Next(2, lista2.Count);
                int contador = 0;

                foreach (var item2 in lista2)
                {
                    contador++;

                    if (contador == val)
                    {
                        equipoDos = item2.dato;
                        break;
                    }
                }
            }
            Torneos a = new Torneos()
            {
                EquipoUno = equipoUno,
                EquipoDos = equipoDos,
                Dep = palabra.ToUpper(),
                Marc1 = 0,
                Marc2 = 0,
                Programado = clFechaTorneo.Text,
            };
            torneos.Add(a);
            manejadorTorneos.Agregar(a);
            dtgTab.ItemsSource = null;
            dtgTab.ItemsSource = manejadorTorneos.Lista;

        }
        private void btnTorneoCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta seguro de cancelar la operación?", "Torneo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                CargarTablas();
                LimpiarTorneo(false);
            }
        }

        private void btnTorneoEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgTab.SelectedItem != null)
            {
                Torneos a = dtgTab.SelectedItem as Torneos;
                if (MessageBox.Show("Esta realmente seguro de eliminar los equipos: " + a.EquipoUno + " & " + a.EquipoDos, "Torneo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorTorneos.Eliminar(a.Id))
                    {
                        MessageBox.Show("Equipos eliminados", "Torneo", MessageBoxButton.OK, MessageBoxImage.Information);
                        CargarTablas();
                    }
                    else
                    {
                        MessageBox.Show("No se puedo eliminar el los quipos", "Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ningun elemento de la tabla para eliminar\nSeleccione uno", "Torneo Eliminación", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //----------------------------------------------------------------------------------------------------

        private void btnGenerarPartidos_Click(object sender, RoutedEventArgs e)
        {

        }
        //---------------------------------------------------------------------------------

        private void btnUsuarioNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposUsuarios();
            HabilitarBotonesUsuario(true);
            operacion = Accion.Nuevo;

        }

        private void btnUsuarioEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgUsuarios.SelectedItem != null)
            {
                Usuarios b = dtgUsuarios.SelectedItem as Usuarios;
                txbNombreUsuario.Text = b.NombreUsuario;
                txbContrasena.Password = b.contrasena;
                operacion = Accion.Editar;
                HabilitarBotonesUsuario(true);
            }
            else
            {
                MessageBox.Show("No has seleccionado ningun elemento", "Usuario", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        private void btnUsuarioGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombreUsuario.Text) || string.IsNullOrEmpty(txbContrasena.Password))
            {
                MessageBox.Show("Falta datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if(operacion==Accion.Nuevo)
            {
                Usuarios b = new Usuarios()
                {
                    NombreUsuario = txbNombreUsuario.Text,
                    contrasena = txbContrasena.Password,
                };
                if (manejadorUsuario.Agregar(b))
                {
                    MessageBox.Show("Usuario guardado correctamente", "Usuario", MessageBoxButton.OK, MessageBoxImage.Information);
                    CargarTablas();
                    ActualizarTablaUsuarios();
                    LimpiarCamposUsuarios();
                    HabilitarBotonesUsuario(false);
                }
                else
                {
                    MessageBox.Show("No se pudo ingresar los datos correctamente", "Usuario", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Usuarios b = dtgUsuarios.SelectedItem as Usuarios;
                b.NombreUsuario = txbNombreUsuario.Text;
                b.contrasena = txbContrasena.Password;
                if (manejadorUsuario.Modificar(b))
                    {
                    ActualizarTablaUsuarios();
                    LimpiarCamposUsuarios();
                    HabilitarBotonesUsuario(false);
                    MessageBox.Show("Usuario editado correctamente", "Usuarios", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("El usuario no se pudo editar", "Usuario", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnUsuarioCancelar_Click(object sender, RoutedEventArgs e)
        {
           if((MessageBox.Show("Realmente desea cancelar esta operacion","Usuarios", MessageBoxButton.YesNo,MessageBoxImage.Question))==MessageBoxResult.Yes)
            {
                LimpiarCamposUsuarios();
                HabilitarBotonesUsuario(false);
            }
        }

        private void btnUsuarioEliminar_Click(object sender, RoutedEventArgs e)
        {
            Usuarios b = dtgUsuarios.SelectedItem as Usuarios;
            if (b != null)
            {
                if (MessageBox.Show("Realmente quieres eliminar este usuario" + b.NombreUsuario,"Usuarios",MessageBoxButton.YesNo,MessageBoxImage.Question)==MessageBoxResult.Yes)
                {
                    manejadorUsuario.Eliminar(b.Id);
                    ActualizarTablaUsuarios();
                }
            }
            else
            {
                MessageBox.Show("No se ha seleccionado ningun usuario", "Usuarios", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dtgCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnTorneoOrdenar_Click(object sender, RoutedEventArgs e)
        {
            if (cmbTipoDeporteTorneo.SelectedItem == null)
            {
                MessageBox.Show("Error...No ha seleccionado ningun deporte!!!", "Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string palabra = cmbTipoDeporteTorneo.Text;
            if (manejadorEquipos.ContadorDeBuscarEquipos(palabra) <= 1)
            {
                MessageBox.Show("No se puede realizar los torneos con un solo equipo\nAgregue más equipos", "Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (string.IsNullOrEmpty(clFechaTorneo.Text))
            {
                MessageBox.Show("Agregue la fecha programada del torneo", "Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            foreach (var item in manejadorEquipos.Lista)
            {
                if (item.Deporte == palabra)
                {
                    Aleatorios equipos = new Aleatorios();
                    equipos.dato = item.NombreEquipo;
                    lista.Add(equipos);
                }
            }


            if (lista.Count % 2 == 0)
            {
                while (((lista.Count) / 2) > 0)
                {
                    AgregarListaNumerosImpares(palabra);
                }
            }
            else
            {
                while (((lista.Count) / 2) > 0)
                {
                    AgregarListaNumerosImpares(palabra);
                }
                UltimoElemntoRestante(palabra);
            }


            LimpiarTorneo(false);
        }

        private void btnPuntosBuscador_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbDeportePuntoEquipos.Text))
            {
                MessageBox.Show("Seleccione un Deporte", "Puntos Equipos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(clcFechaPuntosEquipos.Text))
            {
                MessageBox.Show("Seleccione la fecha de programación del torneo", "Puntos Equipos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            List<DeporteTemporal> tipo = new List<DeporteTemporal>();
            if (cmbDeportePuntoEquipos.SelectedItem != null)
            {
                foreach (var item in manejadorTorneos.Lista)
                {
                    if (item.Dep == cmbDeportePuntoEquipos.Text && item.Programado == clcFechaPuntosEquipos.Text)
                    {
                        DeporteTemporal TipoDeporteTemporal = new DeporteTemporal()
                        {
                            Equipo1 = item.EquipoUno,
                            Equipo2 = item.EquipoDos,
                            Deporte = item.Dep,
                            F_aProgramado = item.Programado,
                            Id = item.Id.ToString(),
                            Marcador_1 = item.Marc1,
                            Marcador_2 = item.Marc2,
                        };
                        tipo.Add(TipoDeporteTemporal);
                    }
                }
                dtgPuntos.ItemsSource = null;
                dtgPuntos.ItemsSource = tipo;
            }
            else
            {
                MessageBox.Show("Algo salio mal :(\nIntentalo de nuevo", "Puntos Equipos", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnPuntosRegresar_Click(object sender, RoutedEventArgs e)
        {
            dtgPuntos.ItemsSource = null;
            dtgPuntos.ItemsSource = manejadorTorneos.Lista;
        }

        private void btnEliminarPuntosEquipos_Click(object sender, RoutedEventArgs e)
        {
            if (dtgPuntos.SelectedItem != null)
            {
                Torneos a = dtgPuntos.SelectedItem as Torneos;
                if (a != null)
                {
                    if (MessageBox.Show("Esta realmente seguro de eliminar los equipos: " + a.EquipoUno + " & " + a.EquipoDos, "Puntos Torneo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (manejadorTorneos.Eliminar(a.Id))
                        {
                            MessageBox.Show("Equipos eliminados", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            CargarTablas();
                            LimpiarPuntosTorneo();
                            ActualizarTablaTorneos();
                        }
                        else
                        {
                            MessageBox.Show("No se puedo eliminar el quipo", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    DeporteTemporal b = dtgPuntos.SelectedItem as DeporteTemporal;
                    foreach (var item in manejadorTorneos.Lista)
                    {
                        if (b.Id == item.Id.ToString())
                        {
                            if (MessageBox.Show("Esta seguro de eliminar los equipos: " + item.EquipoUno + " & " + item.EquipoDos, "Puntos Torneo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                if (manejadorTorneos.Eliminar(item.Id))
                                {
                                    MessageBox.Show("Equipo eliminado", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Information);
                                    CargarTablas();
                                    LimpiarPuntosTorneo();
                                }
                                else
                                {
                                    MessageBox.Show("No se puedo eliminar el quipo", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ningun elemento de la tabla", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancelarPuntos_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta realmente seguro de cancelar la operación?", "Puntos Torneo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                CargarTablas();
                LimpiarPuntosTorneo();
            }
        }

        private void btnEditarPuntos_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPuntosId.Text))
            {
                MessageBox.Show("No ha seleccionado ningun campo de la tabla para actualizar", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (manejadorTorneos.VerificarSiEsNumero(txtPuntosMarcador1.Text) == 1)
            {
                MessageBox.Show("No se aceptan letras, solo numeros en Marcador 1 ", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (manejadorTorneos.VerificarSiEsNumero(txtPuntosMarcador2.Text) == 1)
            {
                MessageBox.Show("No se aceptan letras, solo numeros en Marcador 2", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int eq1 = 0;
            int eq2 = 0;
            if (int.Parse(txtPuntosMarcador1.Text) > int.Parse(txtPuntosMarcador2.Text))
            {
                eq1 = 3;
                eq2 = 1;
            }
            if (int.Parse(txtPuntosMarcador1.Text) < int.Parse(txtPuntosMarcador2.Text))
            {
                eq1 = 1;
                eq2 = 3;
            }
            if (int.Parse(txtPuntosMarcador1.Text) == int.Parse(txtPuntosMarcador2.Text))
            {
                eq1 = 2;
                eq2 = 2;
            }

            foreach (var item in manejadorTorneos.Lista)
            {
                if (item.Id.ToString() == txtPuntosId.Text)
                {
                    item.EquipoUno = txtPuntosEquipo1.Text;
                    item.EquipoDos = txtPuntosEquipo2.Text;
                    item.Marc1 = eq1;
                    item.Marc2 = eq2;
                    if (manejadorTorneos.Modificar(item))
                    {
                        CargarTablas();
                        LimpiarPuntosTorneo();
                        MessageBox.Show("Torneo editado correctamente", "Torneo", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("No se puedo editar correctamente el Torneo", "Torneo", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void LimpiarPuntosTorneo()
        {
            txtPuntosEquipo1.Clear();
            txtPuntosEquipo2.Clear();
            txtPuntosMarcador1.Clear();
            txtPuntosMarcador2.Clear();
            txtPuntosId.Clear();
        }
        private void dtgPuntos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dtgPuntos.SelectedItem != null)
            {
                Torneos a = dtgPuntos.SelectedItem as Torneos;
                if (a != null)
                {
                    txtPuntosId.Text = a.Id.ToString();
                    txtPuntosEquipo1.Text = a.EquipoUno;
                    txtPuntosEquipo2.Text = a.EquipoDos;
                    txtPuntosMarcador1.Text = a.Marc1.ToString();
                    txtPuntosMarcador2.Text = a.Marc2.ToString();
                }
                else
                {
                    DeporteTemporal b = dtgPuntos.SelectedItem as DeporteTemporal;
                    foreach (var item in manejadorTorneos.Lista)
                    {
                        if (b.Id == item.Id.ToString())
                        {
                            txtPuntosId.Text = item.Id.ToString();
                            txtPuntosEquipo1.Text = item.EquipoUno;
                            txtPuntosEquipo2.Text = item.EquipoDos;
                            txtPuntosMarcador1.Text = item.Marc1.ToString();
                            txtPuntosMarcador2.Text = item.Marc2.ToString();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ningun elemento de la tabla", "Puntos Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

       

        
    }
}

