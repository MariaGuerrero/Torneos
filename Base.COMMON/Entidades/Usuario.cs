﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Entidades
{
    public class Usuarios : Base
    {
        public string NombreUsuario { get; set; }
        public string contrasena { get; set; }

        public override string ToString()
        {
            return NombreUsuario;
        }
    }
    
}
