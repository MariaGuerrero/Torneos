﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Entidades
{
   public class Equipos:Base
    {
        public string NombreEquipo { get; set; }
        public string NombreCapitan { get; set; }
        public string Deporte { get; set; }
    }
}
