﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Entidades
{
    class Puntuacion
    {
        public string Tipo_Deporte { get; set; }
        public string Nombre_Equipo { get; set; }
        public string Historial { get; set; }
    }
}
