﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Entidades
{
    public class DeporteTemporal
    {
        public string Id { get; set; }
        public string F_aProgramado { get; set; }
        public string Deporte { get; set; }
        public string Equipo1 { get; set; }
        public int Marcador_1 { get; set; }
        public string Equipo2 { get; set; }
        public int Marcador_2 { get; set; }
    }
}
