﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Entidades
{
    class VistaDeporteEspecifico
    {
        public string Nombre { get; set; }
        public string Deporte { get; set; }
    }
}
