﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Entidades
{
    class Persona:Base
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Domicilio { get; set; }
    }
}
