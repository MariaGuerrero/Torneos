﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Entidades
{
    class Integrantes
    {
        public string Nombre { get; set; }
        public string Ape_Paterno { get; set; }
        public string Ape_Materno { get; set; }
        public string Telefono { get; set; }
        public string Domicilio { get; set; }
        public int Edad { get; set; }
    }
}
