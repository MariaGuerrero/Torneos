﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON
{
    public class Deportes:Base
    {
        public string Deporte { get; set; }
        public override string ToString()
        {
            return Deporte;
        }
    }
}
