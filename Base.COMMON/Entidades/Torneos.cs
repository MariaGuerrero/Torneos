﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Entidades
{
    public class Torneos:Base
    {
        public string EquipoUno { get; set; }
        public string EquipoDos { get; set; }
        public string Dep { get; set; }
        public int Marc1 { get; set; }
        public int Marc2 { get; set; }
        public string Programado { get; set; }
    }
}
