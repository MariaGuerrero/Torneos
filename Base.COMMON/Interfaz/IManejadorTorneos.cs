﻿using Base.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Interfaz
{
    public interface IManejadorTorneos: IManejadorGenerico<Torneos>
    {
        int VerificarSiEsNumero(string text);
    }
}
