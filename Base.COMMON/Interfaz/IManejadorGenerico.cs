﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.COMMON.Interfaz
{
   public  interface IManejadorGenerico<T> where T : Base
    {
        bool Agregar(T entidad);
        List<T> Lista { get; }
        bool Eliminar(ObjectId Id);
        bool Modificar(T entidad);
        T Buscador(ObjectId Id);

    }
}
