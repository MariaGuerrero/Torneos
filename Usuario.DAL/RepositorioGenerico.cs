﻿using Base.COMMON;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usuario.DAL
{
    public class RepositorioGenerico<T> : Base.COMMON.IRepositorio<T> where T : Base.COMMON.Base
    {
        private MongoClient client;
        private IMongoDatabase db;
        public RepositorioGenerico()
        {
            client = new MongoClient(new MongoUrl("mongodb://maria:jesus2525@ds215380.mlab.com:15380/torneo"));
            db = client.GetDatabase("torneo");
        }

        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }

        public List<T> Lista => Collection().AsQueryable().ToList();

        public bool Crear(T entidad)
        {
            try
            {
                Collection().InsertOne(entidad);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(T entidad)
        {
            try
            {
                return Collection().ReplaceOne(p => p.Id == entidad.Id, entidad).ModifiedCount == 1;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(ObjectId Id)
        {
            try
            {
                return Collection().DeleteOne(p => p.Id == Id).DeletedCount == 1;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
