﻿using Base.COMMON;
using Base.COMMON.Entidades;
using Base.COMMON.Interfaz;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usuario.BIZ
{
    public class ManejadorEquipos : IManejadorEquipos
    {
        IRepositorio<Equipos> equipos;
        public ManejadorEquipos(IRepositorio<Equipos> equipos)
        {
            this.equipos = equipos;
        }

        public List<Equipos> Lista => equipos.Lista;

        public bool Agregar(Equipos entidad)
        {
            return equipos.Crear(entidad);
        }

        public int Aleatorios(string palabra)
        {
            int valor = ContadorDeBuscarEquipos(palabra);
            Random a = new Random();
            int v = 0;
            return v = a.Next(1, valor);
        }

        public Equipos Buscador(ObjectId Id)
        {
           return Lista.Where(e => e.Id == Id).SingleOrDefault();
        }

        public int ContadorDeBuscarEquipos(string palabra)
        {
        return Lista.Where(e => e.Deporte == palabra).ToList().Count();
        }

        public bool Eliminar(ObjectId Id)
        {
            return equipos.Eliminar(Id);
        }

        public bool Modificar(Equipos entidad)
        {
           return equipos.Editar(entidad);
        }
    }
}
