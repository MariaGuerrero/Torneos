﻿using Base.COMMON;
using Base.COMMON.Entidades;
using Base.COMMON.Interfaz;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usuario.BIZ
{
    public class ManejadorTorneos:IManejadorTorneos
    {
        IRepositorio<Torneos> torneos;
        public ManejadorTorneos(IRepositorio<Torneos> torneos)
        {
            this.torneos = torneos;
        }
        public List<Torneos> Lista => torneos.Lista;
        public bool Agregar(Torneos entidad)
        {
            return torneos.Crear(entidad);
        }
        public Torneos Buscador(ObjectId Id)
        {
            return Lista.Where(e => e.Id == Id).SingleOrDefault();
        }
        public bool Eliminar(ObjectId Id)
        {
            return torneos.Eliminar(Id);
        }
        public bool Modificar(Torneos entidad)
        {
            return torneos.Editar(entidad);
        }
        public int VerificarSiEsNumero(string text)
        {
            foreach (var item in text)
            {
                if (!(char.IsNumber(item)))
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            return 1;
        }
    }
}
