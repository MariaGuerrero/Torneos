﻿using Base.COMMON;
using Base.COMMON.Entidades;
using Base.COMMON.Interfaz;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usuario.BIZ
{
    public class ManejadorUsuarios : IManejadorUsuario
    {
        IRepositorio<Base.COMMON.Entidades.Usuarios> usuario;

        public ManejadorUsuarios(IRepositorio<Base.COMMON.Entidades.Usuarios> usuario)
        {
            this.usuario = usuario;
        }

        public List<Base.COMMON.Entidades.Usuarios> Lista => usuario.Lista;

        public bool Agregar(Base.COMMON.Entidades.Usuarios entidad)
        {
            return usuario.Crear(entidad);
        }

        public Base.COMMON.Entidades.Usuarios Buscador(ObjectId Id)
        {
            return Lista.Where(e => e.Id == Id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId Id)
        {
            return usuario.Eliminar(Id);
        }

        public bool Modificar(Base.COMMON.Entidades.Usuarios entidad)
        {
            return usuario.Editar(entidad);
        }
    }
}
